﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            
        }

        string operacija;
        double op1, op2, rez;
        int flag=0;

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aplikaciju je izradio brando koch", "About");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "3";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "4";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "5";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "6";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "7";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "8";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "9";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "1";
        }

        private void zbroji_Click(object sender, EventArgs e)
        {
            operacija = "+";
            try
            {
                op1 = Convert.ToDouble(textBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogresan unos operanda", "Greska");
            }
            textBox1.Text = "";
        }

        private void sinus_Click(object sender, EventArgs e)
        {
            operacija = "sin";
            flag = 1;
            try
            {
                op1 = Convert.ToDouble(textBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogresan unos operanda", "Greska");
            }

        }

        private void kosinus_Click(object sender, EventArgs e)
        {
            operacija = "cos";
            flag = 1;
            try
            {
                op1 = Convert.ToDouble(textBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogresan unos operanda", "Greska");
            }
        }

        private void tangens_Click(object sender, EventArgs e)
        {
            operacija = "tan";
            flag = 1;
            try
            {
                op1 = Convert.ToDouble(textBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogresan unos operanda", "Greska");
            }
        }

        private void logaritam10_Click(object sender, EventArgs e)
        {
            operacija = "log";
            flag = 1;
            try
            {
                op1 = Convert.ToDouble(textBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogresan unos operanda", "Greska");
            }
        }

        private void sqrt_Click(object sender, EventArgs e)
        {
            operacija = "sqrt";
            flag = 1;
            try
            {
                op1 = Convert.ToDouble(textBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogresan unos operanda", "Greska");
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "0";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + ".";
        }

        private void oduzmi_Click(object sender, EventArgs e)
        {
            operacija = "-";
            try
            {
                op1 = Convert.ToDouble(textBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogresan unos operanda", "Greska");
            }
            textBox1.Text = "";
        }

        private void pomnozi_Click(object sender, EventArgs e)
        {
            operacija = "*";
            try
            {
                op1 = Convert.ToDouble(textBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Pogresan unos operanda", "Greska");
            }
            textBox1.Text = "";
        }

        private void CALCULATE_Click(object sender, EventArgs e)//clear
        {
            op1 = 0;
            op2 = 0;
            rez = 0;
            flag = 0;
            textBox1.Text = "";
        }

        private void jednako_Click(object sender, EventArgs e)
        {
            {   if(flag==0)
                {
                    try
                    {
                        op2 = Convert.ToDouble(textBox1.Text);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Pogresan unos operanda", "Greska");
                    }
                }
                
                switch (operacija)
                {
                    case "+":
                        rez = op1 + op2;
                        break;
                    case "-":
                        rez = op1 - op2;
                        break;
                    case "*":
                        rez = op1 * op2;
                        break;
                    case "/":
                        rez = op1 / op2;
                        break;
                    case "log":
                        rez = Math.Log10(op1) ;
                        break;
                    case "cos":
                        rez = Math.Cos(op1);
                        break;
                    case "sin":
                        rez = Math.Sin(op1);
                        break;
                    case "tan":
                        rez = Math.Tan(op1);
                        break;
                    case "sqrt":
                        rez = Math.Sqrt(op1);
                        break;
                    

                }
                flag = 0; 
                textBox1.Text = rez.ToString();
            }
        }
    }
}
