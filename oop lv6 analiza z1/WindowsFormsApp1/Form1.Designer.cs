﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.clear = new System.Windows.Forms.Button();
            this.zbroji = new System.Windows.Forms.Button();
            this.jednako = new System.Windows.Forms.Button();
            this.pomnozi = new System.Windows.Forms.Button();
            this.oduzmi = new System.Windows.Forms.Button();
            this.podijeli = new System.Windows.Forms.Button();
            this.sinus = new System.Windows.Forms.Button();
            this.kosinus = new System.Windows.Forms.Button();
            this.tangens = new System.Windows.Forms.Button();
            this.logaritam10 = new System.Windows.Forms.Button();
            this.sqrt = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(878, 38);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(56, 34);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(154, 34);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(82, 34);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(97, 188);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 80);
            this.button1.TabIndex = 1;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(206, 188);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 80);
            this.button2.TabIndex = 2;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(319, 188);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(80, 80);
            this.button3.TabIndex = 4;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(97, 295);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(80, 80);
            this.button4.TabIndex = 6;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(206, 295);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(80, 80);
            this.button5.TabIndex = 8;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(319, 295);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(80, 80);
            this.button6.TabIndex = 9;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(97, 401);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(80, 80);
            this.button7.TabIndex = 10;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(206, 401);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(80, 80);
            this.button8.TabIndex = 11;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(319, 401);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(80, 80);
            this.button9.TabIndex = 12;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(24, 110);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(374, 29);
            this.textBox1.TabIndex = 13;
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(437, 110);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(75, 53);
            this.clear.TabIndex = 14;
            this.clear.Text = "CLR";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.CALCULATE_Click);
            // 
            // zbroji
            // 
            this.zbroji.Location = new System.Drawing.Point(442, 207);
            this.zbroji.Name = "zbroji";
            this.zbroji.Size = new System.Drawing.Size(70, 61);
            this.zbroji.TabIndex = 15;
            this.zbroji.Text = "+";
            this.zbroji.UseVisualStyleBackColor = true;
            this.zbroji.Click += new System.EventHandler(this.zbroji_Click);
            // 
            // jednako
            // 
            this.jednako.Location = new System.Drawing.Point(543, 115);
            this.jednako.Name = "jednako";
            this.jednako.Size = new System.Drawing.Size(158, 48);
            this.jednako.TabIndex = 16;
            this.jednako.Text = "=";
            this.jednako.UseVisualStyleBackColor = true;
            this.jednako.Click += new System.EventHandler(this.jednako_Click);
            // 
            // pomnozi
            // 
            this.pomnozi.Location = new System.Drawing.Point(442, 420);
            this.pomnozi.Name = "pomnozi";
            this.pomnozi.Size = new System.Drawing.Size(70, 61);
            this.pomnozi.TabIndex = 17;
            this.pomnozi.Text = "*";
            this.pomnozi.UseVisualStyleBackColor = true;
            this.pomnozi.Click += new System.EventHandler(this.pomnozi_Click);
            // 
            // oduzmi
            // 
            this.oduzmi.Location = new System.Drawing.Point(442, 314);
            this.oduzmi.Name = "oduzmi";
            this.oduzmi.Size = new System.Drawing.Size(70, 61);
            this.oduzmi.TabIndex = 18;
            this.oduzmi.Text = "-";
            this.oduzmi.UseVisualStyleBackColor = true;
            this.oduzmi.Click += new System.EventHandler(this.oduzmi_Click);
            // 
            // podijeli
            // 
            this.podijeli.Location = new System.Drawing.Point(543, 207);
            this.podijeli.Name = "podijeli";
            this.podijeli.Size = new System.Drawing.Size(70, 61);
            this.podijeli.TabIndex = 19;
            this.podijeli.Text = "/";
            this.podijeli.UseVisualStyleBackColor = true;
            // 
            // sinus
            // 
            this.sinus.Location = new System.Drawing.Point(543, 314);
            this.sinus.Name = "sinus";
            this.sinus.Size = new System.Drawing.Size(70, 61);
            this.sinus.TabIndex = 20;
            this.sinus.Text = "sin";
            this.sinus.UseVisualStyleBackColor = true;
            this.sinus.Click += new System.EventHandler(this.sinus_Click);
            // 
            // kosinus
            // 
            this.kosinus.Location = new System.Drawing.Point(543, 420);
            this.kosinus.Name = "kosinus";
            this.kosinus.Size = new System.Drawing.Size(70, 61);
            this.kosinus.TabIndex = 21;
            this.kosinus.Text = "cos";
            this.kosinus.UseVisualStyleBackColor = true;
            this.kosinus.Click += new System.EventHandler(this.kosinus_Click);
            // 
            // tangens
            // 
            this.tangens.AutoEllipsis = true;
            this.tangens.Location = new System.Drawing.Point(641, 207);
            this.tangens.Name = "tangens";
            this.tangens.Size = new System.Drawing.Size(70, 61);
            this.tangens.TabIndex = 22;
            this.tangens.Text = "tan";
            this.tangens.UseVisualStyleBackColor = true;
            this.tangens.Click += new System.EventHandler(this.tangens_Click);
            // 
            // logaritam10
            // 
            this.logaritam10.Location = new System.Drawing.Point(641, 314);
            this.logaritam10.Name = "logaritam10";
            this.logaritam10.Size = new System.Drawing.Size(70, 61);
            this.logaritam10.TabIndex = 23;
            this.logaritam10.Text = "log";
            this.logaritam10.UseVisualStyleBackColor = true;
            this.logaritam10.Click += new System.EventHandler(this.logaritam10_Click);
            // 
            // sqrt
            // 
            this.sqrt.Location = new System.Drawing.Point(641, 420);
            this.sqrt.Name = "sqrt";
            this.sqrt.Size = new System.Drawing.Size(70, 61);
            this.sqrt.TabIndex = 24;
            this.sqrt.Text = "sqrt";
            this.sqrt.UseVisualStyleBackColor = true;
            this.sqrt.Click += new System.EventHandler(this.sqrt_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(24, 188);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(58, 187);
            this.button10.TabIndex = 25;
            this.button10.Text = "0";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(24, 401);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(58, 80);
            this.button11.TabIndex = 26;
            this.button11.Text = ".";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(437, 497);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(264, 25);
            this.label1.TabIndex = 27;
            this.label1.Text = "*Calculators default is radian ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(878, 610);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.sqrt);
            this.Controls.Add(this.logaritam10);
            this.Controls.Add(this.tangens);
            this.Controls.Add(this.kosinus);
            this.Controls.Add(this.sinus);
            this.Controls.Add(this.podijeli);
            this.Controls.Add(this.oduzmi);
            this.Controls.Add(this.pomnozi);
            this.Controls.Add(this.jednako);
            this.Controls.Add(this.zbroji);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Calculator";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button zbroji;
        private System.Windows.Forms.Button jednako;
        private System.Windows.Forms.Button pomnozi;
        private System.Windows.Forms.Button oduzmi;
        private System.Windows.Forms.Button podijeli;
        private System.Windows.Forms.Button sinus;
        private System.Windows.Forms.Button kosinus;
        private System.Windows.Forms.Button tangens;
        private System.Windows.Forms.Button logaritam10;
        private System.Windows.Forms.Button sqrt;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label1;
    }
}

