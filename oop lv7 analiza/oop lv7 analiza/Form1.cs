﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


/*Napravite igru križić-kružić (iks-oks) korištenjem znanja stečenih na ovoj
laboratorijskoj vježbi. Omogućiti pokretanje igre, unos imena dvaju igrača, ispis
koji igrač je trenutno na potezu, igranje igre s iscrtavanjem križića i kružića na
odgovarajućim mjestima te ispis dijaloga s porukom o pobjedi, odnosno
neriješenom rezultatu kao i praćenje ukupnog rezultata.
 */
namespace oop_lv7_analiza
{
    public partial class Form1 : Form
    {
        Graphics g;
        Pen defaultPen = new Pen(Color.Black);
        int turnCounter = 0;
        int[,] matrix = new int[3,3];
        int endFlag=0;
        int scoreO = 0;
        int scoreX = 0;
        int scorePlayer1=0;
        int scorePlayer2=0;
        string namePlayer1;
        string namePlayer2;
        char pastTurn = 'X';
        
        
        

        public Form1()
        {
            InitializeComponent();
            g = pictureBox1.CreateGraphics();
            pictureBox1.Enabled = false;
            btnNewGame.Enabled = false;
        }

        interface IShape
        {
            void draw(Graphics g, Pen p, int x, int y);
        }

        class shapeO : IShape
        {
            int r;
            public shapeO()
            {
                r = 100;

            }
            public void draw(Graphics g, Pen p, int x, int y)
            {
                g.DrawEllipse(p, x - r / 2, y - r / 2, r, r);
            }
        }

        class shapeX : IShape
        {
            public void draw(Graphics g, Pen p, int x, int y)
            {
                Point pointA = new Point(x + 20, y + 20);
                Point pointB = new Point(x - 20, y - 20);
                Point pointC = new Point(x + 20, y - 20);
                Point pointD = new Point(x - 20, y + 20);

                g.DrawLine(p, pointA, pointB);
                g.DrawLine(p, pointC, pointD);
            }
        }

        private void btnNewGame_Click(object sender, EventArgs e)
        {
            pictureBox1.Enabled = true;
            turnCounter = 0;
            endFlag = 0;
            g.Clear(Color.White);
            

            if(pastTurn=='X')
            {
                label1.Text = "its O's turn";
                pastTurn = 'O';
            }
            else
            {
                label1.Text = "its X's turn";
                pastTurn = 'X';
            }
            #region draw grid
            Point point1 = new Point(150, 20);
            Point point2 = new Point(300, 20);

            Point point3 = new Point(150, 450);
            Point point4 = new Point(300, 450);

            Point point5 = new Point(20, 150);
            Point point6 = new Point(20, 300);

            Point point7 = new Point(450, 150);
            Point point8 = new Point(450, 300);

            g.DrawLine(defaultPen, point1, point3);
            g.DrawLine(defaultPen, point2, point4);
            g.DrawLine(defaultPen, point5, point7);
            g.DrawLine(defaultPen, point6, point8);


            #endregion

            for (int i=0; i < 3; i++)
            {
                for (int j=0; j < 3;j++)
                {
                    matrix[i, j] = -1;
                 }
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {   
            if(turnCounter%2==0)
            {
                shapeO shape = new shapeO();
                #region determine the click location
                //rubovi
                if (e.X < 150 && e.Y < 150)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[0, 0] = 0;
                }
                if (e.X > 300 && e.Y < 150)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[0,2] = 0;
                }
                if (e.X < 150 && e.Y > 300)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[2, 0] = 0;
                }
                if (e.X > 300 && e.Y > 300)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[2, 2] = 0;
                }

                //gore
                if (e.Y < 150 && e.X > 150 && e.X < 300)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[0, 1] = 0;
                }

                //dolje
                if (e.Y > 300 && e.X > 150 && e.X < 300)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[2, 1] = 0;
                }

                //lijevo
                if (e.Y < 300 && e.Y > 150 && e.X < 150)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[1, 0] = 0;
                }

                //desno
                if (e.Y < 300 && e.Y > 150 && e.X > 300)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[1, 2] = 0;
                }

                //sredina
                if (e.Y < 300 && e.Y > 150 && e.X > 150 && e.X < 300)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[1, 1] = 0;
                }
                #endregion
            }
            else
            {
                shapeX shape = new shapeX();
                #region determine the click location
                if (e.X < 150 && e.Y < 150)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[0, 0] = 1;
                }
                if (e.X > 300 && e.Y < 150)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[0, 2] = 1;
                }
                if (e.X < 150 && e.Y > 300)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[2, 0] = 1;
                }
                if (e.X > 300 && e.Y > 300)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[2, 2] = 1;
                }

                //gore
                if (e.Y < 150 && e.X > 150 && e.X < 300)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[0, 1] = 1;
                }

                //dolje
                if (e.Y > 300 && e.X > 150 && e.X < 300)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[2, 1] = 1;
                }

                //lijevo
                if (e.Y < 300 && e.Y > 150 && e.X < 150)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[1, 0] = 1;
                }

                //desno
                if (e.Y < 300 && e.Y > 150 && e.X > 300)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[1, 2] = 1;
                }

                //sredina
                if (e.Y < 300 && e.Y > 150 && e.X > 150 && e.X < 300)
                {
                    shape.draw(g, defaultPen, e.X, e.Y);
                    matrix[1, 1] = 1;
                }
                #endregion
            }

            turnCounter++;
            #region check if somebody won
            int counterX = 0;
            int counterO = 0;
            //retci
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if(matrix[i,j]==1)
                    {
                        counterX++;
                        counterO = 0;
                    }
                    else if(matrix[i,j]==0)
                    {
                        counterO++;
                        counterX=0;
                    }

                    if(counterO==3 || counterX==3)
                    {
                        endFlag = 1;
                        break;
                    }
                       
                }
                if (endFlag == 1)
                {
                    break;
                }
                counterO = 0;
                counterX = 0;

            }

            //stupci
            if(endFlag!=1)
            {
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (matrix[j, i] == 1)
                        {
                            counterX++;
                            counterO = 0;
                        }
                        else if (matrix[j, i] == 0)
                        {
                            counterO++;
                            counterX = 0;
                        }

                        if (counterO == 3 || counterX == 3)
                        {
                            endFlag = 1;
                            break;
                        }
                    }
                    if (endFlag == 1)
                    {
                        break;
                    }
                    counterO = 0;
                    counterX = 0;
                }
            }

            //dijagonale
            if (endFlag != 1)
            {
                if(matrix[0,0]==1 && matrix[1,1]==1 && matrix[2,2]==1 )
                {
                    endFlag = 1;
                    counterX = 3;
                }
                else if(matrix[0, 2] == 1 && matrix[1, 1] == 1 && matrix[2, 0] == 1)
                {
                    endFlag = 1;
                    counterX = 3;
                }
                else if(matrix[0, 0] == 0 && matrix[1, 1] == 0 && matrix[2, 2] == 0)
                {
                    endFlag = 1;
                    counterO = 3;
                }
                else if (matrix[0, 2] == 0 && matrix[1, 1] == 0 && matrix[2, 0] == 0)
                {
                    endFlag = 1;
                    counterO = 3;
                }
            }

            if (endFlag == 1)
            {
                label1.Text = "";
                if (counterO == 3)
                {
                    MessageBox.Show("O won", "result");
                    scorePlayer1++;
                    lblPlayerO.Text = namePlayer1 + " score: " + scorePlayer1;
                    pictureBox1.Enabled = false;
                    scoreO++;
                }
                if (counterX == 3)
                {
                    MessageBox.Show("X won", "result");
                    scorePlayer2++;
                    lblPlayerX.Text = namePlayer2 + " score: " + scorePlayer2;
                    pictureBox1.Enabled = false;
                    scoreX++;
                }
                g.Clear(Color.White);

            }
            else if (turnCounter % 9 == 0 )
            {
                label1.Text = "";
                MessageBox.Show("draw", "result");
                g.Clear(Color.White);

                pictureBox1.Enabled = false;
            }
            #endregion




            if (turnCounter % 2 == 0 && pictureBox1.Enabled==true)
            {
                label1.Text = "its O's turn";
            }
            else if(pictureBox1.Enabled==true)
            {
                label1.Text = "its X's turn";
            }

            

            


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            setButtonVisibility();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            setButtonVisibility();
        }

        private void setButtonVisibility()
        {
            if ((textBox1.Text != String.Empty) && (textBox2.Text != String.Empty))
            {
                btnEnter.Enabled = true;
            }
            else
            {
                btnEnter.Enabled = false;
            }
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            namePlayer1 = textBox1.Text;
            namePlayer2 = textBox2.Text;
            scorePlayer1 = 0;
            scorePlayer2 = 0;
            btnNewGame.Enabled = true;
            lblPlayerO.Text = namePlayer1 + " score: 0";
            lblPlayerX.Text = namePlayer2 + " score: 0";
            pastTurn = 'X';
        }
    }
}
