﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//oop lv6 analiza z2\oop lv6 analiza z2\bin\Debug is where the dat.txt is
namespace oop_lv6_analiza_z2
{
    public partial class Form1 : Form
    {

        List<String> listString = new List<String>();
        string path = "dat.txt";
        int guessNo = 10;
        string rndLine;
        public string lbl_guessLine = "";
        

        public Form1()
        {
            InitializeComponent();
  
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lbl_guess.Text = guessNo.ToString();
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null) 
                {
                    listString.Add(line);
                }
                
            }

            Random rnd = new Random();
            int randomInt = rnd.Next(0, listString.Count);
            rndLine = listString[randomInt];

            for (int i = 0; i < rndLine.Count(); i++)
            {
                lbl_guessLine += "_ ";
            }
            lbl_sentence.Text = lbl_guessLine;
        }

       

        

       

        private void btn_submit_Click(object sender, EventArgs e)
        {
            if(tb_letter.Text=="" && tb_sentence.Text!="")
            {
                guessNo--;
                lbl_guess.Text = guessNo.ToString();

                if(tb_sentence.Text==rndLine)
                {
                    lbl_sentence.Text = rndLine;
                    MessageBox.Show("You won!", "Congratulations");
                    Application.Exit();
                }

                if(guessNo==0)
                {
                    MessageBox.Show("Wrong,you are out of guesses", "game over");
                    Application.Exit();
                }
            }
            else if(tb_letter.Text != "" && tb_sentence.Text == "")
            {
                bool flag = false;
                if (tb_letter.Text.Length == 1)
                {
                    for (int i = 0; i < rndLine.Length; i++)
                    {
                    
                        if (tb_letter.Text[0]==rndLine[i])
                        {
                            
                            string insertChar= rndLine[i].ToString();
                            lbl_guessLine = lbl_guessLine.Remove(2*i, 1);
                            lbl_guessLine = lbl_guessLine.Insert(2 * i, insertChar);
                            lbl_sentence.Text = lbl_guessLine;

                            flag = true;
                            
                            
                        }

                    }
                    if(flag==false)
                    {
                        guessNo--;
                        lbl_guess.Text = guessNo.ToString();
                        if (guessNo == 0)
                        {
                            MessageBox.Show("Wrong,you are out of guesses", "game over");
                            Application.Exit();
                        }
                    }
                    
                    
                }
                else
                {
                    MessageBox.Show("You didnt enter a letter", "error");
                }

                if (lbl_sentence.Text == lbl_guessLine.Replace("_", ""))
                {
                    lbl_sentence.Text = rndLine;
                    MessageBox.Show("You won!", "Congratulations");
                    Application.Exit();
                }
            }
            else if(tb_letter.Text != "" && tb_sentence.Text != "")
            {
                MessageBox.Show("you cant enter both the sentence and the letter guess", "error");
            }
        }
    }


}
